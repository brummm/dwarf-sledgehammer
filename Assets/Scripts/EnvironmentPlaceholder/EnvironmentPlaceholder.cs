﻿using UnityEngine;
using System;
using System.Runtime.Remoting;
using System.Collections;
using Enemy;
using System.Collections.Generic;
using Utils;

namespace Level
{
    /// <summary>
    /// Handles the Placeholder for the hole game
    /// </summary>
    public class EnvironmentPlaceholder : MonoBehaviour
    {
        private GameObject[] _placeholders;
        public GameObject[] Placeholders { get; private set; }

        public float minUnavailableTime;
        public float maxUnavailableTime;
        private bool ended;

        private List<PreparedEnemyStruct> enemiesList;
        public List<PreparedEnemyStruct> EnemiesList {
            set {
                // reset the list
                enemiesList = value;
                PutEnemiesOnPlaceholders();
            }
            get {
                return enemiesList;
            }
        }


        public void Awake() {
            ended = false;
            Placeholders = GameObject.FindGameObjectsWithTag("EnemyPlaceholder");
            InstantiateEnemyPlaceholders();
        }

        void InstantiateEnemyPlaceholders() {
            foreach (GameObject ep in Placeholders) {
                ep.AddComponent<EnemyPlaceholder>();
            }
        }

        void RestartEnemyPlaceholders() {
            foreach (GameObject ep in Placeholders) {
                ep.GetComponent<EnemyPlaceholder>().Restart();
            }
        }

        void PutEnemiesOnPlaceholders() {
            int wichPlaceholderWillGetTheRemainder = 0;
            int totalOnPlaceholderThatGotTheRemainder = 0;

            // Random works with integers
            int minUnavailableTimeInt = (int)minUnavailableTime * 100;
            int maxUnavailableTimeInt = (int)maxUnavailableTime * 100;

            int howMuchEnemyWillBeOnEachPlaceholder = enemiesList.Count / Placeholders.Length;
            int remainder = enemiesList.Count % Placeholders.Length;

            if (remainder!=0) {
                // if there is a remaider, then put it on a random placeholder
                wichPlaceholderWillGetTheRemainder = ThreadSafeRandom.ThisThreadsRandom.Next(0, Placeholders.Length);
                totalOnPlaceholderThatGotTheRemainder = howMuchEnemyWillBeOnEachPlaceholder + remainder;
            }



            int currentPlaceholder = 0;
            int qtAddedToPlaceholder = 0;
            EnemyPlaceholder currentEnemyPlaceholderComponent = Placeholders[0].GetComponent<EnemyPlaceholder>();

            foreach (PreparedEnemyStruct enemy in enemiesList)
            {
                if(qtAddedToPlaceholder >= howMuchEnemyWillBeOnEachPlaceholder)
                { 
                    bool condition =
                            remainder == 0
                        ||
                            wichPlaceholderWillGetTheRemainder != currentPlaceholder
                        ||
                            totalOnPlaceholderThatGotTheRemainder == qtAddedToPlaceholder;
                    if (condition)
                    {
                        currentPlaceholder++;
                        qtAddedToPlaceholder = 0;
                        currentEnemyPlaceholderComponent = Placeholders[currentPlaceholder].GetComponent<EnemyPlaceholder>();
                    }
                }
                qtAddedToPlaceholder++;

                currentEnemyPlaceholderComponent.AddEnemyToTheList(
                    enemy.EnemyClass,
                    enemy.Time,
                    (float)ThreadSafeRandom.ThisThreadsRandom.Next(minUnavailableTimeInt, maxUnavailableTimeInt) /100,
                    enemy.Items);
            }
        }

        void Update() {

            if (!ended) {

                // if the amount of placeholders ended is equal to the total of placeholders, then fires up ItensEnded
                int endedEP = 0;
                foreach (GameObject ep in Placeholders) {
                    if (ep.GetComponent<EnemyPlaceholder>().Ended) {
                        endedEP++;
                    }
                }

                if (endedEP == Placeholders.Length) {
                    ended = true;
                    RestartEnemyPlaceholders();
                    OnItensEnded();
                }
            }
        }

        /**************************
        * events
        ***************************/
        /// <summary>
        /// Event Handler for itens ended
        /// </summary>
        public event EventHandler ItensEnded;
        protected virtual void OnItensEnded() {
            if (ItensEnded != null)
                ItensEnded(this, EventArgs.Empty);
        }

        // TODO: verificar se o método acima tem razão de existir
    }
}