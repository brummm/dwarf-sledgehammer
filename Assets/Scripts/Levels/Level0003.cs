﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using System;

namespace Level
{

    public class Level0003 : BaseLevel
    {
        public Level0003() {
            enemyList = new List<EnemyStruct>() {
                new EnemyStruct(typeof(SimpleEnemy), 27),
                new EnemyStruct(typeof(StrongerEnemy), 18),
                new EnemyStruct(typeof(InnocentEnemy), 9, 4)
            };

            minAvailableTime = 5;
            maxAvailableTime = 10;
            minUnavailableTime = 0.2f;
            maxUnavailableTime = 2;
        }
    }

}