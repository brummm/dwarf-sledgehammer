﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using System;

namespace Level
{

    public class Level0004 : BaseLevel
    {
        public Level0004() {
            enemyList = new List<EnemyStruct>() {
                new EnemyStruct(typeof(SimpleEnemy), 9, 3, 6),
                new EnemyStruct(typeof(StrongerEnemy), 18),
                new EnemyStruct(typeof(ShieldedEnemy), 6, 10),
                new EnemyStruct(typeof(InnocentEnemy), 3, 4)
            };

            minAvailableTime = 5;
            maxAvailableTime = 10;
            minUnavailableTime = 0.2f;
            maxUnavailableTime = 2;
        }
    }

}