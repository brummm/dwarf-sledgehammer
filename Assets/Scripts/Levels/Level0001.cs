﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using System;
using Item;

namespace Level
{

    public class Level0001 : BaseLevel
    {
        public Level0001() {

            // default values ---------------------------
            minAvailableTime = 3;
            maxAvailableTime = 6;

            // Adding enemies ---------------------------
            // Simple enemies
            Dictionary<Type, EnemyItemsStruct> itemList = new Dictionary<Type, EnemyItemsStruct>() {
                {  typeof(Coin1), new EnemyItemsStruct(.2f) },
                {  typeof(Coin2), new EnemyItemsStruct(.1f) }
            };
            EnemyStruct simpleEnemy = new EnemyStruct(typeof(SimpleEnemy), 18, 10, itemList);


            // List of enemies of the level ------------
            enemyList = new List<EnemyStruct>() {
                simpleEnemy
            };

            
        }

        override public void StartLevel()
        {
            base.StartLevel();
        }
    }

}