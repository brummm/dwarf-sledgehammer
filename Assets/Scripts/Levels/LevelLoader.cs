﻿using UnityEngine;
using System;
using Enemy;
using System.Runtime.Remoting;
using System.Collections;

namespace Level
{
    /// <summary>
    /// Responsible for loading and unloading the levels
    /// </summary>
    public class LevelLoader : MonoBehaviour
    {

        private int maximumLevel = 4;

        /// <summary>
        /// The current level name will, by default, be it's number plus 1
        /// </summary>
        private int _currentLevel = 1;
        public int CurrentLevel
        {
            get { return _currentLevel; }
            set {
                _currentLevelInstance = null;
                _currentLevel = value;
            }
        }                

        /// <summary>
        /// Environment Placeholder Prefab name
        /// </summary>
        private string environmentPPrefabName;

        private EnvironmentPlaceholder environmentPlaceholder;
        public EnvironmentPlaceholder EnvironmentPlaceholder
        {
            get { return environmentPlaceholder; }
            private set { environmentPlaceholder = value; }
        }


        /// <summary>
        /// To be used as the instance of current level
        /// </summary>
        private BaseLevel _currentLevelInstance;
        public BaseLevel CurrentLevelInstance
        {
            get {
                if (_currentLevelInstance == null)
                {
                    _currentLevelInstance = Load(CurrentLevel);
                    _currentLevelInstance.LevelEnded += this.OnLevelEnded;
                }
                return _currentLevelInstance;
            }
        }

        public BaseLevel Load(int index)
        {
            String levelName = AssemblyLevelName(index);
            Type mt = Type.GetType(levelName);
            if (mt != null) {
                BaseLevel instance = (BaseLevel)Activator.CreateInstance(Type.GetType(levelName));
                return instance;
            } else {
                return null;
            }
        }

        

        /// <summary>
        /// Event Listener
        /// </summary>
        public void OnLevelEnded(object source, EventArgs e) {

            bool isLevelValid = SetCurrentLevelToTheNext();

            // start the next level if exists
            if (isLevelValid) {
                StartCurrentLevel();
            } else {
                FinishGame();
            }
        }

        public void StartCurrentLevel() {
            SetupLevel();
            //ShowCurrentLevelOnHud();
            CurrentLevelInstance.StartLevel();
            Debug.Log("comecou level " + CurrentLevel);
        }

        private void ShowCurrentLevelOnHud() {
            // TODO: mostrar o nível atual e a mensagem de "boas vindas" do mesmo
            throw new NotImplementedException();
        }

        private void FinishGame() {
            // TODO: finalizar o jogo e dizer que o cara é o cara
            // TODO: fazer também com que o jogador possa pedir por mais níveis com um formulário ou coisa do tipo (posso até requisitar doações \o/)

            OnLevelEnded();
        }

        public void SetupLevel()
        {
            LoadCurrentLevelEnvironmentPlaceholder();
            
            // adds the reference to the GameObject of the EnvironmentPlaceholder to the current level instance
            CurrentLevelInstance.EnvironmentPlaceholder = environmentPlaceholder;

            // fires the event
            OnEnvironmentPlaceholderAdded();
        }


        

        /// <summary>
        /// Loads the current EnvironmentPlaceholder on the scene
        /// </summary>
        private EnvironmentPlaceholder LoadCurrentLevelEnvironmentPlaceholder() {
            if (IsCurrentLevelEnvironmentPrefabDifferent()) {
                RemoveCurrentEnvironmentPlaceholder();
                environmentPPrefabName = CurrentLevelInstance.EnvironmentPlaceholderPrefabName;
                GameObject environmentPlaceholderGO =
                    Instantiate(Resources.Load<GameObject>("Prefabs/EnvironmentPlaceholders/" + environmentPPrefabName)) as GameObject;
                environmentPlaceholder = environmentPlaceholderGO.GetComponent<EnvironmentPlaceholder>();
                return environmentPlaceholder;
            } else {
                EnvironmentPlaceholder envPlaceholder = environmentPlaceholder;
                environmentPlaceholder.Awake();
                return envPlaceholder;
            }
        }

        private void RemoveCurrentEnvironmentPlaceholder() {
            if (environmentPlaceholder != null) {
                OnEnvironmentPlaceholderRemoved();
                Destroy(environmentPlaceholder);
            }
        }

        private bool IsCurrentLevelEnvironmentPrefabDifferent() {
            return environmentPPrefabName != CurrentLevelInstance.EnvironmentPlaceholderPrefabName;
        }

        bool SetCurrentLevelToTheNext() {
            CurrentLevel++;
            return IsLevelIndexValid(CurrentLevel);
        }

        private String AssemblyLevelName(int index) {
            String str = "Level.Level" + index.ToString("D4");
            return str;
        }


        bool IsLevelIndexValid(int index) {
            if (index > maximumLevel) return false;
            else return true;
        }


        /*********************************************************
        * EVENTS
        **********************************************************/
        public event EventHandler EnvironmentPlaceholderAdded;
        protected virtual void OnEnvironmentPlaceholderAdded() {
            if (EnvironmentPlaceholderAdded != null)
                EnvironmentPlaceholderAdded(this, EventArgs.Empty);
        }

        public event EventHandler EnvironmentPlaceholderRemoved;
        protected virtual void OnEnvironmentPlaceholderRemoved() {
            if (EnvironmentPlaceholderRemoved != null)
                EnvironmentPlaceholderRemoved(this, EventArgs.Empty);
        }

        public event EventHandler LevelEnded;
        protected virtual void OnLevelEnded() {
            if (LevelEnded != null)
                LevelEnded(this, EventArgs.Empty);
        }


    }
}