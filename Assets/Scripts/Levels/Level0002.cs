﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using System;
using Item;

namespace Level
{

    public class Level0002 : BaseLevel
    {
        public Level0002() {

            // default values ---------------------------
            minAvailableTime = 5;
            maxAvailableTime = 10;

            // Adding enemies ---------------------------
            // Simple enemies
            Dictionary<Type, EnemyItemsStruct> simpleEnemyItemList = new Dictionary<Type, EnemyItemsStruct>() {
                {  typeof(Coin1), new EnemyItemsStruct(.5f) },
                {  typeof(Coin2), new EnemyItemsStruct(.2f) },
                {  typeof(Coin3), new EnemyItemsStruct(.1f) }
            };
            EnemyStruct simpleEnemy = new EnemyStruct(typeof(SimpleEnemy), 18, simpleEnemyItemList);


            // Stronger enemies
            Dictionary<Type, EnemyItemsStruct> strongerEnemyItemList = new Dictionary<Type, EnemyItemsStruct>() {
                {  typeof(Coin3), new EnemyItemsStruct(.5f) }
            };
            EnemyStruct strongerEnemy = new EnemyStruct(typeof(StrongerEnemy), 9, strongerEnemyItemList);




            // List of enemies of the level ------------
            enemyList = new List<EnemyStruct>() {
                simpleEnemy,
                strongerEnemy
            };
        }
    }

}