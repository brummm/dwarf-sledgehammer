﻿using UnityEngine;
using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Utils
{
    public class PPSerialization
    {
        public static BinaryFormatter binaryFormatter = new BinaryFormatter();

        public static void Save(string key, object source) {
            MemoryStream memoryStream = new MemoryStream();
            binaryFormatter.Serialize(memoryStream, source);
            PlayerPrefs.SetString(
                key,
                System.Convert.ToBase64String(memoryStream.ToArray())
                );
        }

        public static object Load(string key) {
            string temp = PlayerPrefs.GetString(key);
            if(temp == string.Empty) {
                return null;
            }
            MemoryStream memoryStream = new MemoryStream(System.Convert.FromBase64String(temp));
            return binaryFormatter.Deserialize(memoryStream);

        }

        public static bool HasKey(string key) {
            return PlayerPrefs.HasKey(key);
        }
    }
}