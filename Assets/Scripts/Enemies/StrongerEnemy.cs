﻿using UnityEngine;
using System.Collections;

namespace Enemy
{
    public class StrongerEnemy : BaseEnemy
    {
        void Awake()
        {
            HealthPoints = 2;
            HitPoints = 1;
        }
    }
}