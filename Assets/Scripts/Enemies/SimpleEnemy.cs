﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Item;

namespace Enemy
{
    public class SimpleEnemy : BaseEnemy
    {
        void Awake()
        {
            HealthPoints = 1;
            HitPoints = 1;
        }
    }
}