﻿using UnityEngine;
using System.Collections;
using Utils;

namespace Enemy
{
    public class ShieldedEnemy : BaseEnemy
    {
        private float shieldedTime = 2f;
        private float unshieldedTime = 2f;

        private bool shielded = false;
        private float elapsedTime = 0;

        public override bool CouldBeHit {
            get { return !shielded; }
        }

        void Start() {
            HealthPoints = 1;
            HitPoints = 1;
            RandomizeInitialElapsedTime();
        }

        /// <summary>
        /// To add a incertainty on the behavior of the enemy
        /// </summary>
        void RandomizeInitialElapsedTime() {
            float rndNumber = (float)ThreadSafeRandom.ThisThreadsRandom.Next(0, (int)unshieldedTime * 100);
            elapsedTime = rndNumber / 100;
        }

        void Update() {
            elapsedTime += Time.deltaTime;
            if (!shielded) {
                if (elapsedTime >= unshieldedTime) {
                    elapsedTime = 0;
                    Shield();
                }
            } else {
                if (elapsedTime >= shieldedTime) {
                    elapsedTime = 0;
                    Unshield();
                }
            }
        }

        void Unshield() {
            shielded = false;
            gameObject.transform.localScale = new Vector3(1, 1, 1); //temp
        }

        void Shield() {
            shielded = true;
            gameObject.transform.localScale = new Vector3(1.6f, 1.6f, 1.6f); //temp
            
        }


        public override void GetHit(int hitPoints) {
            if(!shielded) {
                base.GetHit(hitPoints);
            }
        }
    }

    
}