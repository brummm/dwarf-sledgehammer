﻿using UnityEngine;
using System.Collections;

namespace Weapon
{
    [System.Serializable]
    public class Weapon1 : BaseWeapon
    {
        public Weapon1()
        {
            HitPoints = 1;
            ChargedAttackHitPoints = 2;
            TimeBeforeChargedAttack = 0.5f;
        }
    }
}