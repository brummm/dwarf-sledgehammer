﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Item
{
    public class Coin2 : BaseCoin, IItem {

        protected override void Start() {
            amount = 2;
            prefabName = "Coin2";
            base.Start();
        }

    }
}