﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Utils;

namespace Item
{
    public class BaseCoin : BaseItem, IItem, IHitable {
        public int amount;

        protected string prefabName;
        private string prefabFolder = "Prefabs/Items/Coins/";
        private string prefabCraftedName;

        private GameObject prefab;

        protected virtual void Start() {
            prefabCraftedName = prefabFolder + prefabName;
        }


        public void Spawn(GameObject ep) {
            LoadPrefab(ep);
        }

        private void LoadPrefab(GameObject ep) {
            prefab = Instantiate(Resources.Load<GameObject>(prefabName)) as GameObject;
            prefab.transform.position = ep.transform.position;
            prefab.transform.parent = ep.transform;
        }

        public override void GetHit(int hitPoints) {
            Inventory.AddCoins(amount);
            OnGotHit();
        }
    }
}