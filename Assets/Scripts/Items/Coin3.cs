﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Item
{
    public class Coin3 : BaseCoin, IItem {

        protected override void Start() {
            amount = 3;
            prefabName = "Coin3";
            base.Start();
        }

    }
}