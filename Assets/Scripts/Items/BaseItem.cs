﻿using UnityEngine;
using System.Collections;
using System;
using Utils;

namespace Item
{
    public class BaseItem : MonoBehaviour
    {
        void Start() {

        }

        public virtual void GetHit(int hitPoints) {
            OnGotHit(hitPoints);
        }

        /*
        TODO: fazer um publicador de eventos
        public event EventHandler<ItemCollectEventArgs> ItemCollect;
        protected virtual void OnItemCollect() {
            if (ItemCollect != null)
                ItemCollect(this, new ItemCollectEventArgs() { HitPoints = enemyHitPoints });
        }*/

        public event EventHandler<GotHitEventArgs> GotHit;
        public void OnGotHit(int hitPoints=0) {
            if (GotHit != null)
                GotHit(this, GotHitEventArgs.Empty);
        }

    }
}