﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Item
{
    public class Coin1 : BaseCoin, IItem {

        protected override void Start() {
            amount = 1;
            prefabName = "Coin1";
            base.Start();
        }

    }
}