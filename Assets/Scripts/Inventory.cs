﻿using UnityEngine;
using System;
using System.Collections;
using Weapon;
using Utils;

[System.Serializable]
public static class Inventory
{
    private static int lifeSlots;
    private static BaseWeapon weapon;
    private static int currentLevel;
    private static int coins;
    private static int biggestCombo = 0;


    public static int LifeSlots {
        get {
            if (lifeSlots == 0 && PlayerPrefs.HasKey("lifeSlots")) {
                lifeSlots = PlayerPrefs.GetInt("lifeSlots");
            }
            return lifeSlots;
        }
        set {
            lifeSlots = value;
            PlayerPrefs.SetInt("lifeSlots", value);
        }
    }


    public static BaseWeapon Weapon {
        get {
            if (weapon == null && PPSerialization.HasKey("weapon")) {
                weapon = (BaseWeapon)PPSerialization.Load("weapon");
            }
            return weapon;
        }
        set {
            weapon = value;
            PPSerialization.Save("weapon", value);
        }
    }


    public static int CurrentLevel {
        get {
            if (currentLevel == 0 && PlayerPrefs.HasKey("currentLevel")) {
                currentLevel = PlayerPrefs.GetInt("currentLevel");
            }
            return currentLevel;
        }
        set {
            currentLevel = value;
            PlayerPrefs.SetInt("currentLevel", value);
        }
    }

    public static int Coins {
        get {
            if (coins == 0 && PlayerPrefs.HasKey("coins")) {
                coins = PlayerPrefs.GetInt("coins");
            }
            return coins;
        }
        set {
            coins = value;
            PlayerPrefs.SetInt("coins", value);
        }
    }

    public static void AddCoins(int amount) {
        Coins += amount;
    }

    public static void RemoveCoins(int amount) {
        Coins -= amount;
    }


    public static int BiggestCombo {
        get { return biggestCombo;  }
        set {
            if(value > biggestCombo) {
                biggestCombo = value;
            }
        }
    }

    /// <summary>
    /// Check if Inventory has persisted it's values
    /// </summary>
    public static bool Persisted {
        get {
            return PlayerPrefs.HasKey("currentLevel");
        }
    }


}
