﻿using System;
using UnityEngine;
using Enemy;
using Weapon;
using Utils;
using System.Collections.Generic;
using System.Collections;

namespace Player
{
    public class PlayerAvatar : MonoBehaviour
    {

        private Health health;
        public int HealthPoints {
            get { return health.HealthPoints; }
        }
        private BaseWeapon currentWeapon;

        /// <summary>
        /// The amount of damage that the player suffers on a miss
        /// </summary>
        private readonly int damageSufferedOnMiss = 1;


        /// <summary>
        /// It stores every placeholder touchdown time (OnEnemyPlaceholderHitDown) to calculate its difference at the moment of touch up (OnEnemyPlaceholderHitUp)
        /// </summary>
        private Dictionary<string, float> touchDownUpTime = new Dictionary<string, float>();

        private GameObject playerAvatarPrefab;
        private float avatarAlphaAnimationTime = 1f;
        private float avatarMoveAnimationTime = 0.08f;



        void Start() {
            health = new Health();
            health.Slots = Inventory.LifeSlots;
            health.Restore();
            currentWeapon = new Weapon1();
            LoadPlayerAvatarPrefab();
            OnPlayerAvatarStart();
        }

        void LoadPlayerAvatarPrefab() {
            playerAvatarPrefab =
                Instantiate(Resources.Load<GameObject>("Prefabs/Player/PlayerAvatar")) as GameObject;

            // setting all renderers on prefab to be invisible
            PutInitialState();
        }

        public void PutInitialState() {
            playerAvatarPrefab.transform.position = new Vector3(0, 0, 0);
            Renderer[] renderers = playerAvatarPrefab.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers) {
                r.material.color = new Color(
                    r.material.color.r,
                    r.material.color.g,
                    r.material.color.b,
                    0);
            }
        }

        public void OnEnemyPlaceholderHitDown(object source, EventArgs e)
        {
            StartChargedAttack((EnemyPlaceholder)source);
        }

        public void OnEnemyPlaceholderHitStay(object source, EventArgs e) {
            EnemyPlaceholder ep = (EnemyPlaceholder)source;
            float progression = 1f;

            // inform the progression, that is the percentage of 
            float elapsedTime = MyTime.realtimeSinceStartup - touchDownUpTime[ep.InstanceId];
            if (elapsedTime < currentWeapon.TimeBeforeChargedAttack) {
                progression = elapsedTime / currentWeapon.TimeBeforeChargedAttack;
            }
            OnPlayerAvatarHitStay(progression);
        }

        public void OnEnemyPlaceholderHitUp(object source, EventArgs e)
        {
            FinishChargedAttack((EnemyPlaceholder)source);
        }

        public void OnEnemyHide(object source, EnemyHideEventArgs e) {
            GetHit(e.HitPoints);
        }

        private void StartChargedAttack(EnemyPlaceholder ep) {
            touchDownUpTime[ep.InstanceId] = MyTime.realtimeSinceStartup;
            
            // TODO: setar um event publisher aqui para poder fazer feedback de uma animação iniciando
        }

        private void FinishChargedAttack(EnemyPlaceholder ep)
        {
            // TODO: setar um event publisher aqui para poder fazer feedback de uma animação finalizando
            if (ep.IsSomethingAvailableToHit()) {
                if (ep.CouldPlaceholderBeHit()) {
                    int hitPoints = currentWeapon.Attack(MyTime.realtimeSinceStartup - touchDownUpTime[ep.InstanceId]);
                    ep.Hit(hitPoints);
                    AnimateAttack(currentWeapon.LastAttackWasCharged, ep);
                    if (currentWeapon.LastAttackWasCharged)
                        OnPlayerAvatarHitCharged(hitPoints);
                    else
                        OnPlayerAvatarHit(hitPoints);
                }
            } else {
                Miss();
            }
        }

        private void Miss() {
            GetHit(damageSufferedOnMiss);
            OnPlayerAvatarMiss();
        }
        

        public void GetHit(int hitPoints) {
            health.RemoveHealthPoint(hitPoints);
            OnPlayerAvatarGetHit();
            if (health.GotToDie())
            {
                Die();
            }
        }

        void Die() {
            OnPlayerAvatarDie();
        }

        public void OnLevelEnded(object source, EventArgs e) {
            touchDownUpTime.Clear();
        }

        /// <summary>
        /// To manage Enemy Placeholders events subscription
        /// </summary>
        Dictionary<string, bool> subscribed = new Dictionary<string, bool>();

        /// <summary>
        /// Managing Enemy Placeholders events subscription
        /// </summary>
        /// <param name="ep">EnemyPlaceholder instance</param>
        public void SubscribeEnemyPlaceholderEvents(EnemyPlaceholder ep) {
            if (!subscribed.ContainsKey(ep.InstanceId)) {
                ep.EnemyPlaceholderHitDown += OnEnemyPlaceholderHitDown;
                ep.EnemyPlaceholderHitStay += OnEnemyPlaceholderHitStay;
                ep.EnemyPlaceholderHitUp += OnEnemyPlaceholderHitUp;
                ep.EnemyHide += OnEnemyHide;
                
                subscribed[ep.InstanceId] = true;
            }
        }

        public void UnsubscribeEnemyPlaceholderEvents(EnemyPlaceholder ep) {
            if (subscribed.ContainsKey(ep.InstanceId)) {
                ep.EnemyPlaceholderHitDown -= OnEnemyPlaceholderHitDown;
                ep.EnemyPlaceholderHitStay -= OnEnemyPlaceholderHitStay;
                ep.EnemyPlaceholderHitUp -= OnEnemyPlaceholderHitUp;
                ep.EnemyHide -= OnEnemyHide;

                subscribed.Remove(ep.InstanceId);
            }
        }


        /**************************************************************
         *    EVENTS
         **************************************************************/
        /// <summary>
        /// Event Handler for awake
        /// </summary>
        public event EventHandler PlayerAvatarStart;
        protected virtual void OnPlayerAvatarStart() {
            if (PlayerAvatarStart != null)
                PlayerAvatarStart(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for miss
        /// </summary>
        public event EventHandler PlayerAvatarMiss;
        protected virtual void OnPlayerAvatarMiss() {
            if (PlayerAvatarMiss != null)
                PlayerAvatarMiss(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for die
        /// </summary>
        public event EventHandler PlayerAvatarDie;
        protected virtual void OnPlayerAvatarDie() {
            if (PlayerAvatarDie != null)
                PlayerAvatarDie(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for hit
        /// </summary>
        public event EventHandler<PlayerAvatarHitEventArgs> PlayerAvatarHit;
        protected virtual void OnPlayerAvatarHit(int hitPoints) {
            if (PlayerAvatarHit != null) {
                PlayerAvatarHitEventArgs pahea = new PlayerAvatarHitEventArgs() {
                    Charged = true,
                    HitPoints = hitPoints
                };
                PlayerAvatarHit(this, pahea);
            }
        }

        /// <summary>
        /// Event Handler for hit stay
        /// </summary>
        public event EventHandler PlayerAvatarHitStay;
        protected virtual void OnPlayerAvatarHitStay(float progression) {
            if (PlayerAvatarHitStay != null)
                PlayerAvatarHitStay(this, new HitProgressionEventArgs() { Progression = progression });
        }

        /// <summary>
        /// Event Handler for hit charged
        /// </summary>
        public event EventHandler<PlayerAvatarHitEventArgs> PlayerAvatarHitCharged;
        protected virtual void OnPlayerAvatarHitCharged(int hitPoints) {
            if (PlayerAvatarHitCharged != null) {
                PlayerAvatarHitEventArgs pahea = new PlayerAvatarHitEventArgs() {
                    Charged = true,
                    HitPoints = hitPoints
                };
                PlayerAvatarHitCharged(this, pahea);
            }
        }

        /// <summary>
        /// Event Handler for get hit
        /// </summary>
        public event EventHandler PlayerAvatarGetHit;
        protected virtual void OnPlayerAvatarGetHit() {
            if (PlayerAvatarGetHit != null)
                PlayerAvatarGetHit(this, EventArgs.Empty);
        }


        /**************************************************************
         *    VISUAL FEEDBACK
         **************************************************************/

        void AnimateAttack(bool charged, EnemyPlaceholder ep) {
            StopCoroutine("FlashAndFadeOff");
            StartCoroutine("FlashAndFadeOff");

            if (currentAnimateToFinalPosition != ep.transform.position) {
                currentAnimateToFinalPosition = ep.transform.position;
                StopCoroutine("AnimateTo");
                StartCoroutine(AnimateTo(ep.transform.position));                
            }
        }

        IEnumerator FlashAndFadeOff() {
            Renderer[] renderers = playerAvatarPrefab.GetComponentsInChildren<Renderer>();
            foreach (Renderer r in renderers) {
                Color matColor = r.material.color;
                Color colorFrom = new Color(matColor.r, matColor.g, matColor.b, 1);
                Color colorTo = new Color(matColor.r, matColor.g, matColor.b, 0);
                r.material.color = colorFrom;
                for (float t = 0.0f; t < 1f; t += Time.deltaTime / avatarAlphaAnimationTime) {
                    r.material.color = Color.Lerp(colorFrom, colorTo, t);
                    yield return null;
                }
            }
        }

        private Vector3 currentAnimateToFinalPosition = new Vector3();
        IEnumerator AnimateTo(Vector3 position) {
            Vector3 initialPosition = playerAvatarPrefab.transform.position;
            Vector3 finalPosition = new Vector3(position.x, position.y, position.z-1.1f);
            for (float t = 0.0f; t < 1f; t += Time.deltaTime / avatarMoveAnimationTime) {
                playerAvatarPrefab.transform.position = Vector3.Lerp(initialPosition, finalPosition, t);
                yield return null;
            }
        }



    }

    public class PlayerAvatarHitEventArgs : EventArgs
    {
        public bool Charged = false;
        public int HitPoints;
    }

}


