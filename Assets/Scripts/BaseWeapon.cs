﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Weapon
{
    [System.Serializable]
    public class BaseWeapon
    {
        private int hitPoints;
        public int HitPoints
        {
            get { return hitPoints; }
            set { hitPoints = value; }
        }

        private int chargedAttackHitPoints;
        public int ChargedAttackHitPoints
        {
            get { return chargedAttackHitPoints;  }
            set { chargedAttackHitPoints = value; }
        }

        /// <summary>
        /// Do not set this if the 
        /// </summary>
        private float timeBeforeChargedAttack;
        public float TimeBeforeChargedAttack
        {
            get { return timeBeforeChargedAttack;  }
            set { timeBeforeChargedAttack = value; }
        }

        public bool HasChargedAttack
        {
            get { return timeBeforeChargedAttack != 0; }
        }

        public bool LastAttackWasCharged {
            get; private set;
        }
        public int Attack(float timeCharged)
        {
            int hitPoints = HitPoints;
            LastAttackWasCharged = false;
            if (HasChargedAttack && timeCharged >= timeBeforeChargedAttack) {
                hitPoints = ChargedAttackHitPoints;
                LastAttackWasCharged = true;
            }
            return hitPoints;
        }
    }
}