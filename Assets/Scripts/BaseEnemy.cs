﻿using UnityEngine;
using System.Collections;
using Weapon;
using System;
using Item;
using System.Collections.Generic;
using Utils;
using System.Linq;

namespace Enemy
{
    public class BaseEnemy : MonoBehaviour, IHitable
    {

        private int inflictedDamageOnGetHit;
        public int InflictedDamageOnGetHit {
            get { return inflictedDamageOnGetHit; }
            set { inflictedDamageOnGetHit = value;  }
        }



        private bool couldBeHit = true;
        public virtual bool CouldBeHit {
            get { return couldBeHit;  }
        }

        private Dictionary<Type, EnemyItemsStruct> itemsDictionary;
        public Dictionary<Type, EnemyItemsStruct> ItemsDictionary {
            set {
                itemsDictionary = value;
                SelectItemToSpawn();
            }
        }

        protected Type selectedItem;
        protected BaseItem ItemInstance;



        /// <summary>
        /// How much a enemy can take
        /// </summary>
        private int _healthPoints;
        public int HealthPoints
        {
            get { return _healthPoints; }
            set { _healthPoints = value; }
        }

        private int _hitPoints;
        public int HitPoints {
            get { return _hitPoints;  }
            set { _hitPoints = value; }
        }
        

        private void SelectItemToSpawn() {
            if (itemsDictionary != null && itemsDictionary.Count() != 0) {
                float maximumOdd = (float)ThreadSafeRandom.ThisThreadsRandom.Next(0, 101) / 100f;
                float accumulatedOdds = 0;
                float oddsUpperBound = 0;
                foreach (KeyValuePair<Type, EnemyItemsStruct> item in itemsDictionary) {
                    oddsUpperBound = item.Value.AvailabilityOdds;
                    if (accumulatedOdds < maximumOdd && maximumOdd <= oddsUpperBound) {
                        selectedItem = item.Key;
                        break;
                    }
                    accumulatedOdds += oddsUpperBound;
                }
            }
        }

        public void GoAway() {
            // animar e depois da animação usar
            Destroy(gameObject);
            //Destroy(gameObject, 3f); opcional, três segundos de delay para destruir o gameObject

            /*
             ver também esta possibilidade:
             * anim = GetComponent<Animation>();

            anim.Play();

            // Wait for the animation to finish.
            yield WaitForSeconds(anim.clip.length);
             */
        }

        public bool IsDead() {
            return HealthPoints <= 0;
        }

        

        public virtual void GetHit(int hitPoints)
        {
            HealthPoints = HealthPoints - hitPoints;

            // firing up event
            OnGotHit(hitPoints);

            if (IsDead())
                GetRot();
        }

        void GetRot()
        {
            OnGotRot();
            GoAway();
        }



        /***************************************
         * EVENTS
         ***************************************/
        public event EventHandler<GotHitEventArgs> GotHit;
        public void OnGotHit(int hitPoints)
        {
            if (GotHit != null) {
                // only adds a combo if the player do not take damage on hitting the enemy (assuming that it is a innocent one)
                bool enemyTookDamage = (inflictedDamageOnGetHit == 0);
                GotHit(this,
                    new GotHitEventArgs() {
                        TookDamage = enemyTookDamage
                    });
            }
        }

        public event EventHandler<EnemyGotRotEventArgs> GotRot;
        public void OnGotRot()
        {
            EnemyGotRotEventArgs eventArgs = new EnemyGotRotEventArgs();
            if(selectedItem!=null)
                eventArgs.Item = selectedItem;

            // TODO: aleatorizar o surgimento de um item, spawnar ele e passar a instancia para o eventarg abaixo
            // if sorteado então adiciona a instancia na instancia acima

            if (GotRot != null)
                GotRot(this, eventArgs);
        }

        

    }

    public class EnemyGotRotEventArgs : EventArgs
    {
        public Type Item;
    }
}