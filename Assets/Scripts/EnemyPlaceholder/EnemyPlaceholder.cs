﻿using Item;
using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Enemy
{
    public class EnemyPlaceholder : MonoBehaviour
    {
        public string InstanceId {
            get; private set;
        }

        /// <summary>
        /// waiting = true conta o tempo de espera
        /// waiting = false conta o tempo de exibição do inimigo
        /// </summary>
        bool waiting = true;
        bool ended = false;
        bool OnEnemyPlaceholderEndedEventAlreadyFired = false;
        public bool Ended {
            get { return ended; }
        }

        float elapsedTime = 0;
        readonly float timeToPlayHidingAnimation = 0.6f;
        public bool hidingAnimationStarted = false;

        private int currentEnemyIndex = 0;
        private List<EnemyPlaceholderCollectionItem> listOfEnemies;
        public void AddEnemyToTheList(Type enemyClass, float availableTime, float timeBefore, Dictionary<Type, EnemyItemsStruct> items = null)
        {
            if( items!=null ) {

            }
            listOfEnemies.Add(new EnemyPlaceholderCollectionItem(
                enemyClass,
                availableTime,
                timeBefore,
                items
                ));
        }



        public BaseEnemy EnemyInstance;
        public BaseItem ItemInstance;


        void Awake()
        {
            InstanceId = Guid.NewGuid().ToString();
            Setup();
        }

        public void Restart()
        {
            Setup();
        }

        /// <summary>
        /// Initialization and Reinitialization routine
        /// </summary>
        void Setup() {
            ended = false;
            OnEnemyPlaceholderEndedEventAlreadyFired = false;
            currentEnemyIndex = 0;
            elapsedTime = 0;
            listOfEnemies = new List<EnemyPlaceholderCollectionItem>();
        }

        void Update()
        {
            if(listOfEnemies.Count == currentEnemyIndex) {
                ended = true;
                if (OnEnemyPlaceholderEndedEventAlreadyFired) {
                    OnEnemyPlaceholderEnded();
                    OnEnemyPlaceholderEndedEventAlreadyFired = true;
                }
            } else if (!ended) {
                elapsedTime += Time.deltaTime;
                if (listOfEnemies.Count > 0)
                {
                    if (waiting)
                    {
                        if (elapsedTime > listOfEnemies[currentEnemyIndex].TimeBefore)
                        {
                            StopWaiting();
                            SpawnEnemy();
                        }
                    }
                    else
                    {
                        if (!IsSomethingAvailableToHit() && !hidingAnimationStarted) {
                            // if there is nothing to hit anymore,
                            // set elapsedTime to the value that it should be
                            // to start the hiding animation
                            elapsedTime = listOfEnemies[currentEnemyIndex].AvailableTime - timeToPlayHidingAnimation + .05f;
                        }
                        float timeDifference = listOfEnemies[currentEnemyIndex].AvailableTime - elapsedTime;

                        if (timeDifference <= 0) {
                            StarWaiting();
                            HideEnemy();
                            currentEnemyIndex++;
                        } else if(timeDifference <= timeToPlayHidingAnimation) {
                            if(!hidingAnimationStarted)
                                StartHidingEnemy();
                        }
                    }
                }
            }
        }

        void StarWaiting() {
            elapsedTime = 0;
            waiting = true;
        }

        void StopWaiting() {
            elapsedTime = 0;
            waiting = false;
        }

        void SpawnEnemy()
        {
            LoadCurrentEnemy();
            hidingAnimationStarted = false;
        }


        void StartHidingEnemy() {
            hidingAnimationStarted = true;
            if (EnemyInstance != null) {
                // plays the hide EnemyPlaceholder animation
                // that animation must take (timeToPlayHidingAnimation - 0.1)  seconds
                
                // TODO: set animation


                // temp code below
                EnemyInstance.gameObject.transform.localScale = new Vector3(.2f, .2f, .2f);
            }
        }

        /// <summary>
        /// Enemy Hiding, not necessarily died
        /// </summary>
        void HideEnemy()
        {
            if (EnemyInstance != null) {
                if (!EnemyInstance.IsDead())
                    OnEnemyHide(EnemyInstance.HitPoints);

                UnloadCurrentEnemy();
            }

            if (ItemInstance != null) {
                UnloadItem();
            }
        }

        void LoadCurrentEnemy()
        {
            GameObject enemyGO =
                Instantiate(Resources.Load<GameObject>("Prefabs/Enemies/" + listOfEnemies[currentEnemyIndex].EnemyClass.Name)) as GameObject;
            enemyGO.transform.position = gameObject.transform.position + new Vector3(0, 0, -.1f);
            enemyGO.transform.parent = gameObject.transform;
            EnemyInstance = enemyGO.GetComponent<BaseEnemy>();

            EnemyInstance.ItemsDictionary = listOfEnemies[currentEnemyIndex].Items;

            EnemyInstance.GotRot += OnEnemyGotRot;
            EnemyInstance.GotHit += InstanceIndexes.Hud.OnEnemyGotHit;
        }

        void UnloadCurrentEnemy()
        {
            if (EnemyInstance != null)
            {
                Destroy(EnemyInstance.gameObject);
                EnemyInstance = null;
            }
        }

        public bool IsSomethingAvailableToHit()
        {
            return EnemyInstance != null || ItemInstance != null;
        }

        public bool CouldPlaceholderBeHit() {
            return 
                (EnemyInstance!=null && EnemyInstance.CouldBeHit)
                || ItemInstance != null;
        }


        /// <summary>
        /// Hits a enemy placeholder. Returns true if has something to hit, false otherwise
        /// </summary>
        /// <returns></returns>
        public bool Hit(int hitPoints)
        {
            if(!IsSomethingAvailableToHit()) {
                return false;
            }

            if(EnemyInstance!=null) {
                EnemyInstance.GetHit(hitPoints);
            } else {
                ItemInstance.GetHit(hitPoints);
            }
            

            return true;
        }


        void LoadItem(Type selectedItem) {
            GameObject itemGO =
                Instantiate(Resources.Load<GameObject>("Prefabs/Items/" + selectedItem.Name)) as GameObject;
            Debug.Log(itemGO);
            itemGO.transform.position = gameObject.transform.position + new Vector3(0, 0, -.1f);
            itemGO.transform.parent = gameObject.transform;
            ItemInstance = itemGO.GetComponent<BaseItem>();

            ItemInstance.GotHit += OnItemGotHit;
        }

        void UnloadItem() {
            Destroy(ItemInstance.gameObject);
            ItemInstance = null;
        }







        void OnTouchDown()
        {
            OnEnemyPlaceholderHitDown();
        }

        void OnTouchUp()
        {
            OnEnemyPlaceholderHitUp();
        }

        void OnTouchStay()
        {
            OnEnemyPlaceholderHitStay();
        }

        void OnTouchExit()
        {
            OnEnemyPlaceholderHitCancel();
        }


        /***************************************
         * EVENTS
         ***************************************/
        // SUBSCRIBERS
        void OnEnemyGotRot(object source, EnemyGotRotEventArgs e) {
            EnemyInstance = null;
            if(e.Item!=null) {
                LoadItem(e.Item);
            }
        }

        void OnItemGotHit(object source, EventArgs e) {
            // TODO: tem mais algo a fazer aqui?
            UnloadItem();
        }


        // PUBLISHERS

        /// <summary>
        /// Event Handler Enemy Hided
        /// </summary>
        public event EventHandler<EnemyHideEventArgs> EnemyHide;
        protected virtual void OnEnemyHide(int enemyHitPoints) {
            if (EnemyHide != null)
                EnemyHide(this, new EnemyHideEventArgs() { HitPoints = enemyHitPoints });
        }


        /// <summary>
        /// Event Handler for touch input down
        /// </summary>
        public event EventHandler EnemyPlaceholderHitDown;
        protected virtual void OnEnemyPlaceholderHitDown()
        {
            if (EnemyPlaceholderHitDown != null)
                EnemyPlaceholderHitDown(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for touch input stay
        /// </summary>
        public event EventHandler EnemyPlaceholderHitStay;
        protected virtual void OnEnemyPlaceholderHitStay() {
            if (EnemyPlaceholderHitStay != null)
                EnemyPlaceholderHitStay(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for touch input up
        /// </summary>
        public event EventHandler EnemyPlaceholderHitUp;
        protected virtual void OnEnemyPlaceholderHitUp()
        {
            if (EnemyPlaceholderHitUp != null)
                EnemyPlaceholderHitUp(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for touch input exit
        /// </summary>
        public event EventHandler EnemyPlaceholderHitCancel;
        protected virtual void OnEnemyPlaceholderHitCancel()
        {
            if (EnemyPlaceholderHitCancel != null)
                EnemyPlaceholderHitCancel(this, EventArgs.Empty);
        }

        /// <summary>
        /// Event Handler for when there is no more items or enemies on the placeholder
        /// </summary>
        public event EventHandler EnemyPlaceholderEnded;
        protected virtual void OnEnemyPlaceholderEnded() {
            if (EnemyPlaceholderEnded != null)
                EnemyPlaceholderEnded(this, EventArgs.Empty);
        }

    }
    /*

            ItemsDictionary = new Dictionary<Type, EnemyItemsStruct>() {
                { typeof(Coin1), new EnemyItemsStruct(.1f) } // 10 percent chances
            };*/

    public struct EnemyPlaceholderCollectionItem
    {
        public Type EnemyClass;
        public float AvailableTime;
        public float TimeBefore;
        public readonly Dictionary<Type, EnemyItemsStruct> Items;

        public EnemyPlaceholderCollectionItem(Type enemyClass, float availableTime, float timeBefore, Dictionary<Type, EnemyItemsStruct> items) {
            this.EnemyClass = enemyClass;
            this.AvailableTime = availableTime;
            this.TimeBefore = timeBefore;
            this.Items = items;
        }

        public EnemyPlaceholderCollectionItem(Type enemyClass, float availableTime, float timeBefore)
        {
            this.EnemyClass = enemyClass;
            this.AvailableTime = availableTime;
            this.TimeBefore = timeBefore;
            this.Items = null;
        }

    }
}