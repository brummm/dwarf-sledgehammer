﻿using UnityEngine;
using System.Collections;
using Level;
using Player;
using Enemy;
using Utils;
using Weapon;
using System;

/// <summary>
/// This is the core class to the game.
/// It will load all the others classes "procedurally".
/// </summary>
public class ProceduralLevel : MonoBehaviour {

    private LevelLoader levelLoaderInstance;
    private PlayerAvatar playerAvatarInstance;
    private Hud hudInstance;

    void Awake()
    {
        gameObject.AddComponent<LevelLoader>();
        levelLoaderInstance = gameObject.GetComponent<LevelLoader>();
    }


    void Start () {
        if (!Inventory.Persisted) {
            LoadDefaultInventory();
        }
        SetupHud();
        SetupPlayer();
        SetupEvents();
        levelLoaderInstance.StartCurrentLevel();
    }

    void Pause()
    {
        //http://answers.unity3d.com/questions/7544/how-do-i-pause-my-game.html
        MyTime.Pause();
    }

    void Unpause()
    {
        MyTime.UnPause();
    }

    void LoadDefaultInventory() {
        Inventory.LifeSlots = 6;
        Inventory.Weapon = new Weapon1();
        Inventory.CurrentLevel = 1; // temp
    }


    void SetupPlayer()
    {
        playerAvatarInstance = gameObject.AddComponent<PlayerAvatar>();
    }


    void SetupHud() {
        hudInstance = InstanceIndexes.Hud;
    }

    /// <summary>
    /// Here we will set the publishers and the subscribers to the events
    /// </summary>
    private void SetupEvents()
    {
        levelLoaderInstance.EnvironmentPlaceholderAdded += OnEnvironmentPlaceholderAdded;
        levelLoaderInstance.EnvironmentPlaceholderRemoved += OnEnvironmentPlaceholderRemoved;
        levelLoaderInstance.LevelEnded += playerAvatarInstance.OnLevelEnded;


        playerAvatarInstance.PlayerAvatarStart += hudInstance.OnPlayerAvatarStart;
        playerAvatarInstance.PlayerAvatarMiss += hudInstance.OnPlayerAvatarMiss;
        playerAvatarInstance.PlayerAvatarGetHit += hudInstance.OnPlayerAvatarGetHit;
        //playerAvatarInstance.PlayerAvatarHit += hudInstance.OnPlayerAvatarHit;
        playerAvatarInstance.PlayerAvatarHitCharged += hudInstance.OnPlayerAvatarHitCharged;
    }

    private void OnEnvironmentPlaceholderAdded(object source, EventArgs e)
    {
        LevelLoader llInstance = (LevelLoader)source;
        EnemyPlaceholder epc;
        GameObject[] placeholders = llInstance.EnvironmentPlaceholder.Placeholders;
        if (placeholders != null)
        {
            foreach (GameObject ep in placeholders)
            {
                epc = ep.GetComponent<EnemyPlaceholder>();

                // setting up the player events
                playerAvatarInstance.SubscribeEnemyPlaceholderEvents(epc);
            }
        }
    }

    private void OnEnvironmentPlaceholderRemoved(object source, EventArgs e) {
        LevelLoader llInstance = (LevelLoader)source;
        EnemyPlaceholder epc;
        GameObject[] placeholders = llInstance.EnvironmentPlaceholder.Placeholders;
        if (placeholders != null) {
            foreach (GameObject ep in placeholders) {
                epc = ep.GetComponent<EnemyPlaceholder>();

                // setting up the player events
                playerAvatarInstance.UnsubscribeEnemyPlaceholderEvents(epc);
            }
        }
    }
}
