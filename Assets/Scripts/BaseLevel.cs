﻿using UnityEngine;
using System;
using System.Runtime.Remoting;
using System.Collections;
using System.Collections.Generic;
using Enemy;
using System.Linq;
using Utils;

namespace Level
{
    /// <summary>
    /// Might Mother of all levels
    /// </summary>
    public class BaseLevel
    {
        /// <summary>
        /// Minimum time that a enemy will be available to hit
        /// </summary>
        public float minAvailableTime = 2f;
        /// <summary>
        /// Maximum time that a enemy will be available to hit
        /// </summary>
        public float maxAvailableTime = 4f;

        /// <summary>
        /// Minimum time that a enemy will be UNavailable to hit
        /// </summary>
        public float minUnavailableTime = 1f;
        /// <summary>
        /// Maximum time that a enemy will be UNavailable to hit
        /// </summary>
        public float maxUnavailableTime = 4f;



        private bool levelEnded = false;
        public void Finish() {
            levelEnded = true;
            EnvironmentPlaceholder.ItensEnded -= OnItensEnded;
            OnLevelEnded();
        }

        public bool IsFinished() {
            return levelEnded;
        }


        /// <summary>
        /// Stores the name of the Level's Environment Placeholder Prefab
        /// They are stored in "/Resources/Prefabs/EnvironmentPlaceholders/"
        /// and the name below is a prefab under this path
        /// </summary>
        protected string environmentPlaceholderPrefabName = "Base";
        public string EnvironmentPlaceholderPrefabName {
            get { return environmentPlaceholderPrefabName; }
            set { environmentPlaceholderPrefabName = value; }
        }

        protected EnvironmentPlaceholder environmentPlaceholder;
        public EnvironmentPlaceholder EnvironmentPlaceholder { get; set; }

        /// <summary>
        /// List of enemies
        /// </summary>
        protected List<EnemyStruct> enemyList;
        private List<PreparedEnemyStruct> preparedEnemyList = new List<PreparedEnemyStruct>();

        /// <summary>
        /// Randomize the list of enemies
        /// </summary>
        protected void PrepareEnemyList()
        {
            int randLowerBoundDefault = FloatToIntTimes100(minAvailableTime);
            int randUpperBoundDefault = FloatToIntTimes100(maxAvailableTime);
            int randLowerBoundUsed = 0;
            int randUpperBoundUsed = 0;
            float availableTime = 0;

            foreach (EnemyStruct enemy in enemyList)
            {
                if (!enemy.IsFixedTime()) {
                    // if the enemy has no min availability time, it gets from the level
                    if (enemy.HasMinAvailableTime()) {
                        randLowerBoundUsed = FloatToIntTimes100(enemy.MinAvailableTime);
                    } else {
                        randLowerBoundUsed = randLowerBoundDefault;
                    }

                    // if the enemy has no max availability time, it gets from the level
                    if (enemy.HasMinAvailableTime()) {
                        randUpperBoundUsed = FloatToIntTimes100(enemy.MaxAvailableTime);
                    } else {
                        randUpperBoundUsed = randUpperBoundDefault;
                    }
                }
                for (int i = 0; i < enemy.Quantity; i++)
                {
                    if(enemy.IsFixedTime()) {
                        availableTime = enemy.FixedTime;
                    } else {
                        availableTime = ((float)ThreadSafeRandom.ThisThreadsRandom.Next(randLowerBoundUsed, randUpperBoundUsed) / 100);
                    }
                    preparedEnemyList.Add(new PreparedEnemyStruct(
                        enemy.EnemyClass,
                        availableTime,
                        enemy.Items)
                    );
                }
            }

            // sorting the list of enemies
            preparedEnemyList.Shuffle();
        }

        protected int FloatToIntTimes100(float f) {
            return (int)f * 100;
        }


        /// <summary>
        /// Gets the total placeholders and distributes the amount of enemies evenly between then
        /// </summary>
        protected void DistributeEnemiesOnPlaceholders()
        {
            EnvironmentPlaceholder ep = EnvironmentPlaceholder;
            ep.minUnavailableTime = minUnavailableTime;
            ep.maxUnavailableTime = maxUnavailableTime;
            ep.EnemiesList = preparedEnemyList;
        }


        /// <summary>
        /// Starts the current Level, it's should only be acessed from this class children
        /// </summary>
        virtual public void StartLevel()
        {
            // enemy list
            PrepareEnemyList();
            DistributeEnemiesOnPlaceholders();

            // Events
            SetupEvents();
            

            levelEnded = false;
        }

        private void SetupEvents() {
            EnvironmentPlaceholder.ItensEnded += OnItensEnded;
        }

        private void OnItensEnded(object source, EventArgs e) {
            Finish();
        }

        /**************************
        * events
        ***************************/
        /// <summary>
        /// Event Handler for level ended
        /// </summary>
        public event EventHandler LevelEnded;
        protected virtual void OnLevelEnded()
        {
            if (LevelEnded != null)
                LevelEnded(this, EventArgs.Empty);
        }
    }




    



    /**************************
     * structs
     ***************************/

    

    public struct PreparedEnemyStruct
    {
        public readonly Type EnemyClass;
        public readonly float Time;
        public readonly Dictionary<Type, EnemyItemsStruct> Items;

        public PreparedEnemyStruct(Type enemyClass, float time, Dictionary<Type, EnemyItemsStruct> items)
        {
            this.EnemyClass = enemyClass;
            this.Time = time;
            this.Items = items;
        }
    }
    

}