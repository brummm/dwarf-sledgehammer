﻿using UnityEngine;
using System.Collections;
using System;

namespace Utils
{
    public class GotHitEventArgs : EventArgs
    {
        public static GotHitEventArgs empty;
        public static new GotHitEventArgs Empty {
            get {
                if(empty == null) {
                    empty = new GotHitEventArgs();
                }
                return empty;
            }
        }

        public bool TookDamage;
    }
}