﻿using UnityEngine;
using System.Collections;
using System;

namespace Enemy
{
    public class EnemyHideEventArgs : EventArgs
    {
        public int HitPoints;
    }
}