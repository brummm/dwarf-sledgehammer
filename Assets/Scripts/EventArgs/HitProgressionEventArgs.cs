﻿using UnityEngine;
using System.Collections;
using System;

namespace Player
{
    public class HitProgressionEventArgs : EventArgs
    {
        public float Progression;
    }
}